package com.demo;

import com.demo.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class LoginApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void testRedis(){
        User user = new User();
        user.setUserId("1");
        user.setUserName("Join同学");
        redisTemplate.opsForValue().set("user:1", user);
    }
}
