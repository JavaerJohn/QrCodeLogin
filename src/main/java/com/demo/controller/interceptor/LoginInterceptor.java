//package com.demo.controller.interceptor;
//
//import com.alibaba.fastjson.JSONObject;
//import com.demo.entity.Response;
//import com.demo.utils.TokenUtil;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.PrintWriter;
//
//@Component
//public class LoginInterceptor implements HandlerInterceptor {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        response.setCharacterEncoding("utf-8");
//        String token = request.getHeader("access_token");
//        // token存在
//        if (token != null) {
//            //验证token是否有效
//            return TokenUtil.verifyToken(token);
//        }
//        Response needTokenResponse = Response.createErrorResponse("token无效");
//        responseMessage(response, response.getWriter(), needTokenResponse);
//        return false;
//    }
//
//    private void responseMessage(HttpServletResponse response, PrintWriter out, Response apiResponse) {
//        response.setContentType("application/json; charset=utf-8");
//        out.print(JSONObject.toJSONString(apiResponse));
//        out.flush();
//        out.close();
//    }
//}
