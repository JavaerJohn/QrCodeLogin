package com.demo.utils;

import java.util.UUID;


public class CommonUtil {
    private static final String SPLIT = ":";
    private static final String PREFIX_TICKET = "ticket";
    private static final String PREFIX_USER = "user";

    public static String generateUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String getTicketKey(String uuid) {
        return PREFIX_TICKET + SPLIT + uuid;
    }

    public static String getUserKey(String userId) {
        return PREFIX_USER + SPLIT + userId;
    }
}
